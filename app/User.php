<?php

namespace App;

use App\Usergoals;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'jellyName', 'jellyColor', 'jellyType', 'jellyAge',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function target(){
        return $this->hasMany('App\Target');
    }

    public function goal(){
        return $this->hasMany('App\Goal');
    }
    

    // public function goals() 
    // {
    //     return $this->belongsToMany(Goals::class, Usergoals::class);
    // }
    
    // public function usergoals() {

    //     // return true;
    //     return $this->hasMany('App\Usergoals');
    // }
}
