<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{


    public function create()
    {

    	return view('signup');
    }



    public function store()
   	{
   		//VALIDATING THE THE FORM

   		// $this->validate(request(), [
   		// 	'name' => 'required',
   		// 	'email' => 'required|email',
   		// 	'password' => 'required'
   		// ]);


   		//CREATING A NEW USER
   

   		$user = User::create([
   				'name' => request('name_signup'),
   				'email' => request('email_signup'),
   				'password' => bcrypt(request('pass_signup')),
   				'jellyName' => ' ',
          'jellyColor' => ' ',
          'jellyType' => ' ',
          'jellyAge' => ' '
   		]);

   		//SIGN THEM IN

   		auth()->login($user);


   		//DIRECT TO HOME PAGE

   		return view('newbean');


    }

    public function updateJelly(){
    	//UPDATES THE JELLY NAME ??
    	User::where('id', auth()->user()->id)->update(array('jellyName' => request('jellyName'), 'jellyColor' => request('jellyColor'), 'jellyType' => request('jellyType')));

      return;
    }
}
