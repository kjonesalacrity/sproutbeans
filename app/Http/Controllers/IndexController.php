<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function showIndex(){
    	return view('index');	
    }

    public function showLogin(){
    	return view('login');
    }

    public function showSignup(){
    	return view('signup');
    }

    public function showHomePage(){
        return view('homepage');
    }

    public function showManager(){
        return view('managment');
    }

    //TEMP
    public function showJellys(){
        return view('newbean');
    }
}
