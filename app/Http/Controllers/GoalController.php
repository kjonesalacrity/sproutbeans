<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GoalController extends Controller
{
    //
    public function showAlacrityGoals(Request $req, $operator){
    	$x = $req['x'];
    	$y = $req['y'];
    	$result = 0;

    	switch ($operator) {
    		case 'add':
    			$result = $x + $y;
    			break;
    		case 'sub':
    			$result = $x - $y;
    			break;
    		case 'multi':
    			$result = $x % $y;
    			break;
    		case 'div':
    			$result = $x * $y;
    			break;
    	}

    	return view('calcview', ['resul' => $result]);
    }

    public function bus(Request $req){
         $capacity = $req['capcity'];
         $seated = $req['seated'];
         $waiting =$req['waiting'];

         $freeSeats = $capacity - $seated;

         if (($seated + $waiting) > $capacity) {

            return view('nomore');

         }

         else if ($seated < $capacity) {

            return view('freeseats', compact('freeSeats')); 

        }

        else if ($seated = $capacity) {

            return view('capacity');

         }
    }

    public function showForm(){
        return view('bus');
    }
}
