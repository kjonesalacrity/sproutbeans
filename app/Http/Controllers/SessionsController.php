<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Goal;
use App\Phase;
use App\Progression;
use App\Target;

class SessionsController extends Controller
{
	public function __construct(){

		//$this->middleware('guest', ['except' => 'destory']);

	}

    public function create()
    {
    	return view('login');
    }


    public function store(){

        //GGETTING THE CURRENT PHASE
        $currentDate = date("Y-m-d");

        $currentPhase = Phases::where([
                        ['start_date', '<', $currentDate],
                        ['end_date', '>', $currentDate],
                        ])->first();


        //CHECK THAT THERE IS A PHASE AT THIS CURRENT MOMENT
        if($currentPhase == null){
            return view('nothing');
        }

        //GETTING THE PERCENTAGE OF TIME PASSED
        $daysDone = (strtotime($currentDate) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);
        $overallDays = ( strtotime($currentPhase->end_date) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);

        $percentDays = round($daysDone /  $overallDays * 100);


        //CHECK IF IT IS THE ADMIN THAT IS COMING IN
    	if((request('') == 'admin') && (request('password')=='alacrity2018')){

            //Displays all the users that are that are not admin
    		$users = User::where('jellyName', '<>', '')->get();

            //Find the over all progress for the current phase 
                //Get the goals where the phase number is equal to the current phase
                //Get count each goal (go through loop)
                //Get the last updated progress of those goals
                //Add all those progesses together
                //Divided them by the number of progresses 


            //SEND THEM TO THE MANAGEMENT PAGE WHERE ALL THE BEANS WILL BE SHOWED
            return view('managment', compact('users', 'currentPhase', 'percentDays'));

    	}else{

    		//ELSE CHECK IF THE LOGIN IN IS CORRECT
    		if (! auth()->attempt(request(['email', 'password'])))
		    {
                $error = 'You have errors with you data';
	           	return view('error', compact('error'));
		    };

      
       
           //showHomePage();
            //get all the goals of alacrity: user_id = 1

            $goals = Goals::all();

            $user = User::all();


            $goalsAlacrity = $user->goals->id;
            $goals = App\User::find(1)->goals()->orderBy('goal_name')->get();

            

            // $goalsAlacrity = Goals::where([
            //                     ['user_id', '1'],
            //                     ['phase_id', $currentPhase->id],
            //                     ])->get();

            // $currentUser = User::where('email', request('email'))->first();

            // //Get the last progress per goal
            // $progressUser = Progresses::where('user_id', $currentUser->id)
            //                     ->get();

            // $lastGoalUser = Progresses::orderBy('goal_id', 'desc')->where('user_id', $currentUser->id)
            //                     ->first();


            return view('homepage', compact('goalsAlacrity'));//,'currentPhase', 'percentDays', 'progressUser', 'lastGoalUser'));
               
		   
    	}
    }

    public function showHomePage(){
        $currentDate = date("Y-m-d");

        $currentPhase = Phases::where([
                        ['start_date', '<', $currentDate],
                        ['end_date', '>', $currentDate], 
                        ])->first();

        //CHECK THAT THERE IS A PHASE AT THIS CURRENT MOMENT
        if($currentPhase == null){
            return view('nothing');
        }

        $daysDone = (strtotime($currentDate) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);
        $overallDays = ( strtotime($currentPhase->end_date) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);

        $percentDays = round($daysDone /  $overallDays * 100);

        $goalsAlacrity = Goals::where([
                            ['user_id', '1'],
                            ['phase_id', $currentPhase->id],
                            ])->get();


        return view('homepage', compact('goalsAlacrity','currentPhase', 'percentDays'));
    }

    public function showAlacrity(){

        return view('alacrity');
    }


    public function destroy(){

    	auth()->logout();

    	return redirect('/');
    }


}

