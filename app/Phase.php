<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progression extends Model
{
    protected $fillable = [
        'name', 'start_date', 'end_date', 
    ];

    public function target(){
    	return $this->hasMany('App\Target');
    }

}