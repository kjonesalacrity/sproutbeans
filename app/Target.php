<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = [
        'name', 'start_date', 'end_date', 'phase_id', 'time_in',
    ];

   public function user(){
   		return $this->belongsTo('App\User');
   }

   public function progression(){
   		return $this->hasMany('App\Progression');
   }

   public function phase(){
   		return $this->belongsTo('App\Phase');
   }
}