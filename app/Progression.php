<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progression extends Model
{
    protected $fillable = [
        'target_id', 'user_id', 'progress', 'updated',
    ];

    public function target(){
    	return $this->belongsTo('App\Target');
    }

}