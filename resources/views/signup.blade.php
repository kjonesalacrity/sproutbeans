@extends('layouts.master')

@section('body')
	<div id="inspage">
		<div class="container-fluid">
			<div class="formTops">	
				<!--FORM -->	
				<div class="row">
					<div class="col"></div>
					<div class="col-lg-6 col-md-8 col-sm-12" id="form_outline_login">
						<div class="row">
							<div class="col-lg-1 col-md-4 col-sm-1"></div>
							<div class="col-lg-3">
								<h1>Sprout</h1>
							</div>
						</div>

						

						<div class="row">
							<div class="col-lg-1 col-md-4 col-sm-1"></div>
							<div class="col-lg-7 col-md-8 col-sm-5" >
								<form action="{{ route('signUp.store') }}" method="POST">
		 							@csrf

									<label for="name_signup">FULL NAME: </label>
									<input type="text" name="name_signup" id="name_signup" required>

									<label for="email_signup" class="email_signup">EMAIL: </label>
									<input type="email" name="email_signup" class="email_signup" required>

									<label for="pass_signup" class="pass_signup">PASSWORD: </label>
									<input type="password" name="pass_signup" class="pass_signup" required>

									<br><br>
									<input type="submit" name="submit_login" class="submit_signup" value="SUMBIT">
								</form>
								
							</div>
							<div class="col"></div>
						</div>
					</div>
					<div class="col"></div>
				</div>
			</div>
		</div>
	</div>
@endsection