@extends('layouts.master')

@section('body')
	<div id="sign_up">
		<div class="container-fluid">
			<div class="formTops">	
				<!--FORM -->	
				<div class="row">
					<div class="col"></div>
					<div class="col-lg-6 col-md-8 col-sm-12" id="form_outline_login">
						<div class="row">
							<div class="col-lg-1 col-md-4 col-sm-1"></div>
							<div class="col-lg-3">
								<h1>Sprout</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-1"></div>
							<div class="col-lg-6 col-md-8 col-sm-5" >
								<br><br><br>
								<div class="buttons"><img src="images/signin.svg" class="button_images"><a href="{{ route('login') }}">LOGIN</a></div>
								<br>
								<div class="buttons"><img src="images/login.svg" class="button_images"><a href="{{ route('signUp.create') }}">SIGN UP</a></div>
							</div>
							<div class="col"></div>
						</div>
					</div>
					<div class="col"></div>
				</div>
			</div>
		</div>
	</div>
@endsection