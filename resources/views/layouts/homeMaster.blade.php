@extends('layouts.master')


@section('body')
	

	<nav class="nav flex-column" id="sideNav">
		<a class="nav-link links" href=""><img src="{{ asset('images/bean_logo.svg') }}" class="bean_logo"></a>
		
	  	<a class="nav-link links" href="{{ route('alacrity') }}">HOME</a>
	  	<a class="nav-link links" href="#">ALACRITY</a>
	  	<a class="nav-link links" href="#">MY GOALS</a>

	  	@if (Auth::check())
			<a class="nav-link links" href="{{ route('logout') }}" data-toggle="tooltip" title="Click to Logout">{{ Auth::user()->name }}</a>
		@endif
	</nav>

	@section('content')
	
	@show
	
		
@endsection


