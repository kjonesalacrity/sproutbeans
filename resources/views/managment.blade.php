@extends('layouts.homeMaster')

@section('content')
	<div class="beans"></div>
		<div class="container-fluid">
			  <!-- Modal -->
			  <div class="modal fade" id="myModal" role="dialog">
			    <div class="modal-dialog modal-lg">
			      <div class="modal-content">
			        <div class="modal-header">
			          <h4 class="modal-title">Modal Header</h4>
			        </div>
			        <div class="modal-body">
			          <p>This is where you would dynmically add the data from the user that matches that ID</p>
			        </div>
			        <div class="modal-footer">
			          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
			      </div>
			    </div>
			  </div>

			  <h2>Your <span id="curly">Spourting</span> Beans</h2>
		 	<div class="row">
		 		<div class="col-lg-1 col-md-2 col-sm-3">
		 			
		 		</div>
		 		<div class="col-lg-11 col-md-2 col-sm-3">
		 			<!-- DYNAMICALLY ADD A ROW EACH TIME -->
		 			<div class='row'>
		 				@foreach ($users as $user)
							<div class="col-md-3" id="bean_pods" data-toggle="modal" data-target="#myModal">
							    <div class="card user-info">
							    	<br>
							    	<div class='row'>
							    		<div class="col-md-6">
							    			<div class="bean_pic_mang">
							    				@if( $user->jellyType == "hipster")
								 					@if ( $user->jellyColor == 'pink')
								 						<img src="images/pink_bean_hipster.gif" class="front_bean">
								 					@elseif ( $user->jellyColor == 'green')
								 						<img src="images/green_bean_hipster.gif" class="front_bean">
								 					@elseif ( $user->jellyColor == 'blue')
								 						<img src="images/blue_bean_hipster.gif" class="front_bean">
								 					@else
								 						<img src="images/orange_bean_hipster.gif" class="front_bean">
								 					@endif
								 				@elseif( $user->jellyType == 'normal')
								 					@if ( $user->jellyColor == 'pink')
								 						<img src="images/pink_bean.gif" class="front_bean">
								 					@elseif ( $user->jellyColor == 'green')
								 						<img src="images/green_bean.gif" class="front_bean">
								 					@elseif ( $user->jellyColor == 'blue')
								 						<img src="images/blue_bean.gif" class="front_bean">
								 					@else
								 						<img src="images/orange_bean.gif" class="front_bean">
								 					@endif
								 				@endif
							    			</div>
							    			<h5 class="card-title">{{ $user->name }}</h5>
							    		</div>
							    		<div class="col-md-6">
							    			<h5>Current Phase:</h5>
							    			<h6>{{ $currentPhase->phase_name }}</h6>
							    			<div>Phase Completion:</div>
							    			<div class="progress">
  												<div class="progress-bar" role="progressbar" style="width: {{ $percentDays }}%;" aria-valuenow="{{ $percentDays }}" aria-valuemin="0" aria-valuemax="100">{{ $percentDays }}%</div>
											</div>

											<div>Overall progress of this phase:</div>
							    		</div>
							    	</div>
							    	<div class='row'>
							    		<div class="col-md-6">
							    			<p class="card-text">{{ $user->jellyName }}</p>
									    	<p class="card-text">{{ $user->email }}</p>
							    		</div>
							    		<div class="col-md-6">
							    			<h5>Update</h5>
							    			
							    		</div>
							    	</div>
								</div>
							</div>
						@endforeach
		 			</div>
		 		</div>
		 	</div>
		 	<br>
		</div>
@endsection