@extends('layouts.homeMaster')

@section('content')
	<div class="beans"></div>
		<div class="container-fluid">
			<h5>$goalsAlacrity</h5>
		 	<!-- <div class="row">
		 		<div class="col-lg-6 "></div>
		 		<div class="col-lg-5 " id="bean_homepage">
		 			<br>
		 			<div id="big_bean_homepage"> 
		 				@if( Auth::user()->jellyType == 'hipster')
		 					@if ( Auth::user()->jellyColor == 'pink')
		 						<img src="images/pink_bean_hipster.gif" class="front_bean">
		 					@elseif ( Auth::user()->jellyColor == 'green')
		 						<img src="images/green_bean_hipster.gif" class="front_bean">
		 					@elseif ( Auth::user()->jellyColor == 'blue')
		 						<img src="images/blue_bean_hipster.gif" class="front_bean">
		 					@else
		 						<img src="images/orange_bean_hipster.gif" class="front_bean">
		 					@endif
		 				@elseif( Auth::user()->jellyType == 'normal')
		 					@if ( Auth::user()->jellyColor == 'pink')
		 						<img src="images/pink_bean.gif" class="front_bean">
		 					@elseif ( Auth::user()->jellyColor == 'green')
		 						<img src="images/green_bean.gif" class="front_bean">
		 					@elseif ( Auth::user()->jellyColor == 'blue')
		 						<img src="images/blue_bean.gif" class="front_bean">
		 					@else
		 						<img src="images/orange_bean.gif" class="front_bean">
		 					@endif
		 				@endif
		 				<br>
		 				<img src="images/bean_shaddow.png" class="front_bean_shaddow">
		 			</div>
		 			@if (Auth::check())
						<h3> {{ Auth::user()->jellyName }}</h3>
						<h4> {{ Auth::user()->name }}</h4>

					@endif
		 		</div>
		 		<div class="col-lg-6 col-md-2 col-sm-3">
		 			<br>
		 			<div class="row">
		 				<div class="col-lg-12 col-md-2 col-sm-3">
		 					<div id="graph_div">
		 						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
								  <div class="carousel-inner">
								  		@for ($i = 1; $i <= $lastGoalUser->goal_id; $i++)
								  			<div class="carousel-item">

								  				<script type="text/javascript">
								  					var count = 0;
								  					var countWeek = 0;
								  					var dataProgress = [];
								  					var dataUpdated = [];

								  					if( "{{ $i }}" == "1"){
								  						$(".carousel-item").addClass('active');
								  					}
								  				</script>
								  				@foreach($progressUser->where('goal_id', $i) as $progress)
								  					<script type="text/javascript"> 
								  						dataProgress[count++] = "{{ $progress->percentage }}"; 
								  						dataUpdated[countWeek++] = "{{ $progress->updated }}";
								  					</script>
								  				@endforeach

								  				<canvas id="line-chart{{ $i }}" class="homepage-graphs" height="130vh"></canvas> 

								  				<script type="text/javascript">
								  					new Chart(document.getElementById("line-chart{{ $i }}"), {
													    type: 'line',
													    data: {
													      labels: dataUpdated,
													      datasets: [{ 
													          data: dataProgress,
													          label: "Yours",
													          borderColor: "#EC008B",
													          fill: false
													        }, { 
													          data: [0,20,70,100],
													          label: "Average",
													          borderColor: "#F16521",
													          fill: false
													        }
													      ]
													    },
													    options: {
													      title: {
													        display: true,
													        text: "{{ $currentPhase->phase_name }} : Goal {{ $i }}"
													      }
													    }
													  });
								  				</script>
								  			</div>
								  			<br>
								  		@endfor  
								  </div>
								  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
								    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
								    <span class="carousel-control-next-icon" aria-hidden="true"></span>
								    <span class="sr-only">Next</span>
								  </a>
								</div>
		 						
		 					</div>
		 				</div>
		 			</div>
		 			<br>
		 			
		 			<div class="row">
		 				<div class="col-lg-6 col-md-2 col-sm-3">
		 					<!-- WE ARE GOING TO LOOP THROUGH ALL GOALS AND GIVE THME IN A ROW -->
		 					<div class="list_div">
		 						<h3>Alacrity Goals</h3>
		 						<h6>{{ $currentPhase->phase_name }}</h6>
		 						<div class="progress">
									<div class="progress-bar" role="progressbar" style="width: {{ $percentDays }}%;" aria-valuenow="{{ $percentDays }}" aria-valuemin="0" aria-valuemax="100">{{ $percentDays }}%</div>
								</div>
		 						<br>

		 						<div class="row">
									<div class="col-lg-6 ">
										<h4> GOAL NAMES: </h4>
									</div>
									<div class="col-lg-3 ">
										<h4> PROGRESS: </h4>
									</div>
									<div class="col-lg-3 ">
										<h4> TIME LEFT: </h4>
									</div>
		 						</div>
	 							<br>

		 						@foreach ($goalsAlacrity as $goal)
		 							@foreach($progressUser->where('goal_id', '1')->sortBy('updated', 'desc') as $progress)
				 						<div class="row">
											<div class="col-lg-6 ">
												<div class="goalname">  {{ $goal->goal_name }} </div>
											</div>
											<div class="col-lg-3 ">
												<!-- PROGRESS OF THAT GOAL -->
												<!-- <canvas id="doughnut-chart" width="8" height="8"></canvas>	 -->
												{{ $progress->percentage }}
												 <!-- {{ $progressUser->where('goal_id', $goal->id)->first() }}  -->
												</ol>
												 
											</div>
											<div class="col-lg-3 ">
												<!-- TIME LEFT OF THAT GOAL -->
												<!-- <canvas id="doughnut-chart2" width="8" height="8"></canvas>	 -->
												{{ $goal->end_date }}
											</div>
				 						</div>
			 							<br>
			 						@endforeach
		 						@endforeach
		 					</div>
		 				</div>

		 				<div class="col-lg-6 col-md-2 col-sm-3">
		 					<!-- WE ARE GOING TO LOOP THROUGH ALL GOALS AND GIVE THME IN A ROW -->
		 					<div class="list_div">
		 						<h3>Personal Goals</h3>
		 						<br><br>

		 						<div class="row">
									<div class="col-lg-6 ">
										<h4> GOAL NAMES: </h4>
									</div>
									<div class="col-lg-3 ">
										<h4> PROGRESS: </h4>
									</div>
									<div class="col-lg-3 ">
										<h4> TIME LEFT: </h4>
									</div>
		 						</div>
	 							<br>

		 						@foreach ($goalsAlacrity as $goal)
			 						<div class="row">
										<div class="col-lg-6 ">
											<div class="goalname">  {{ $goal->goal_name }} </div>
										</div>
										<div class="col-lg-3 ">
											<!-- PROGRESS OF THAT GOAL -->
											<canvas id="doughnut-chart" width="8" height="8"></canvas>	
										</div>
										<div class="col-lg-3 ">
											<!-- TIME LEFT OF THAT GOAL -->
											<canvas id="doughnut-chart2" width="8" height="8"></canvas>	
										</div>
			 						</div>
		 						
		 						@endforeach

		 						<br>
		 						<a data-toggle="modal" data-target="#myModal" class="buttons">ADD NEW GOAL</a>
		 						<br>

		 						<!-- MODAL FOR TO ADD NEW GOAL -->
		 						<div class="modal fade" id="myModal" role="dialog">
								    <div class="modal-dialog modal-lg">
								    	<div class="modal-content">
									    	
									    	<div class="heading_form">ADD A NEW PERSONAL GOAL </div>
									    	<form class="personal_form">
									    		<label for="email_login">GOAL NAME: </label>
												<input type="text" name="goal_name_personal" id="goal_name_personal"  required>

												<label for="pass_login">START DATE: </label>
												<input type="date" name="start_date" id="start_date" class="dates" required>

												<label for="pass_login">END DATE: </label>
												<input type="date" name="end_date" id="end_date" class="dates" required>

												<br><br>
												<input type="submit" name="submit_login" id="submit_login" value="SUMBIT">
									    	</form>

									    	<img src="images/bean_bluegreen.svg" class="bean_background">
									    </div>
								    </div>
								</div>

		 					</div>
		 				</div>
		 			</div>
		 			<br>
		 			<div class="row">
		 				<div class="col-lg-12 col-md-2 col-sm-3">
		 					<div id="comments-div">
		 						<h4>Comments</h4>
		 					</div>
		 				</div>
		 			</div>
		 			<br>
		 			<div class="row">
		 				<div class="col-lg-12 col-md-2 col-sm-3">
		 					<div id="graph_div">
		 						<h2> Time in Alacrity </h2>
		 					</div>
		 				</div>
		 			</div>
		 		</div>
		 	</div> -->
		</div>
@endsection

