<?php
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@showIndex')->name("index");

// Route::get('login', 'IndexController@showLogin')->name("login");

// Route::get('signUp', 'IndexController@showSignup')->name("signUp");

Route::get('home', 'IndexController@showHomePage')->name("home");

// Route::get('manager', 'IndexController@showManager')->name("manager");

// Route::post('login/process', 'UsersController@login')->name("login.process");

// Route::post('signUp/process', 'UsersController@signUp')->name("signUp.process");

// Route::get('bus/view', 'GoalController@showForm')->name("bus");

// //to log in! get function to view the login page (show form)
// Route::get('/login/{variableName}', 'GoalController@showAlacrityGoals');

// //to process the data (get the data after the user has put it in)
// Route::post('/loginprocess', 'GoalController@showAlacrityGoals');



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


//MAKING SHIT WORK!!!

Route::get('tinker', function(){
	$user = User::find(1);
	return $user->usergoals->goals->goal_name;
});

Route::get('signUp', 'RegistrationController@create')->name('signUp.create');

Route::post('signUp', 'RegistrationController@store')->name('signUp.store');
Route::get('signup/homepage', 'SessionsController@showHomePage')->name('signup.showhome');

//TEMP:
Route::get('showjelly', 'IndexController@showJellys');

Route::post('jellyup', 'RegistrationController@updateJelly')->name('signup.jellyup'); 



Route::get('login', 'SessionsController@create')->name('login');
Route::post('login', 'SessionsController@store')->name('login.store');
Route::get('alacrity', 'SessionsController@showAlacrity')->name('alacrity');


Route::get('logout', 'SessionsController@destroy')->name('logout');