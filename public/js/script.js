
$(document).ready(function(){
   //createChartWinLoss();

    //DO THE NEW BEAN COLORS
      //1. ORANGE
      $( "#orange" ).click(function() {
        $( "#orange" ).addClass("point_active");
        $( "#pink" ).removeClass("point_active");
        $( "#blue" ).removeClass("point_active");
        $( "#green" ).removeClass("point_active");

        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/orange_bean.gif");
        }
        else{
           $(".new_bean_front").attr("src","images/orange_bean_hipster.gif");
        }

      });

      //2. Pink bean
      $( "#pink" ).click(function() {
        $( "#pink" ).addClass("point_active");
        $( "#orange" ).removeClass("point_active");
        $( "#blue" ).removeClass("point_active");
        $( "#green" ).removeClass("point_active");

        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/pink_bean.gif");
        }
        else{
           $(".new_bean_front").attr("src","images/pink_bean_hipster.gif");
        }

      });

      //3. Green bean
      $( "#green" ).click(function() {
        $( "#green" ).addClass("point_active");
        $( "#orange" ).removeClass("point_active");
        $( "#blue" ).removeClass("point_active");
        $( "#pink" ).removeClass("point_active");

        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/green_bean.gif");
        }
        else{
           $(".new_bean_front").attr("src","images/green_bean_hipster.gif");
        }

      });

      //3. Blue bean
      $( "#blue" ).click(function() {
        $( "#blue" ).addClass("point_active");
        $( "#orange" ).removeClass("point_active");
        $( "#green" ).removeClass("point_active");
        $( "#pink" ).removeClass("point_active");

        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/blue_bean.gif");
        }
        else{
           $(".new_bean_front").attr("src","images/blue_bean_hipster.gif");
        }

      });

      //5. To Hipster
      $( "#hipster" ).click(function() {
          $( "#hipster" ).addClass("point_active");
          $( "#normal" ).removeClass("point_active");
       
          var beanColour = $(".color_bean.point_active").attr('id');
        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/" + beanColour + "_bean.gif");
        }
        else{
          $(".new_bean_front").attr("src","images/"+ beanColour +"_bean_hipster.gif");
        }
      });

       //5. To Normal
      $( "#normal" ).click(function() {
          $( "#normal" ).addClass("point_active");
          $( "#hipster" ).removeClass("point_active");
          var beanColour = $(".color_bean.point_active").attr('id');
        //change the bean gif
        if($('#normal').hasClass('point_active')){
          $(".new_bean_front").attr("src","images/" + beanColour + "_bean.gif");
        }
        else{
           $(".new_bean_front").attr("src","images/"+ beanColour +"_bean_hipster.gif");
        }

      });


      //MAKE SURE THAT IT DOESNT SUMBIT BY ITSELF
      $("#new_bean_form").submit(function(e){
          return false;
      });


      ///SUMBIT THE FOR TO PHP
     //we want to send the jelly name, the color and the type
      $( "#new_bean_submit" ).click(function() {
        //Make sure that jelly name has some input
        var jellyName = $('#jelly_new').val();

        if(jellyName == null){
          $('#jelly_new').css("border-color", "red");
        }
        else{
          var jellyColor = $(".color_bean.point_active").attr('id');
          var jellyType = $(".type_bean.point_active").attr('id');

          sendData(jellyName, jellyColor, jellyType);
        }
        
      });
     
});


function sendData(jName, jColor, jType){
        alert(jName + jColor + jType);
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  ///THE EMAIL FORMAT
      //SEND IT PHP THROUGH AJAX
      var data = {
          'jellyName': jName,
          'jellyColor': jColor,
          'jellyType': jType
      };
      $.ajax({
          type: "POST",
          url: '/jellyup',
          data: data,
          success: function(){
              window.location.href = "/signup/homepage";
          }
      });
}

function createChartWinLoss(){
  //   new Chart(document.getElementById("line-chart"), {
  //   type: 'line',
  //   data: {
  //     labels: ['WEEK 1','WEEK 2','WEEK 3','WEEK 4','WEEK 5','WEEK 6'],
  //     datasets: [{ 
  //         data: [0,10,17,43,75,100],
  //         label: "Yours",
  //         borderColor: "#EC008B",
  //         fill: false
  //       }, { 
  //         data: [0,20,30,40,70,100],
  //         label: "Average",
  //         borderColor: "#F16521",
  //         fill: false
  //       }
  //     ]
  //   },
  //   options: {
  //     title: {
  //       display: true,
  //       text: 'PHASE 1: INSERSION'
  //     }
  //   }
  // });

  // new Chart(document.getElementById("line-chart2"), {
  //   type: 'line',
  //   data: {
  //     labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
  //     datasets: [{ 
  //         data: [86,114,106,106,107,111,133,221,783,2478],
  //         label: "Africa",
  //         borderColor: "#3e95cd",
  //         fill: false
  //       }, { 
  //         data: [282,350,411,502,635,809,947,1402,3700,5267],
  //         label: "Asia",
  //         borderColor: "#8e5ea2",
  //         fill: false
  //       }, { 
  //         data: [168,170,178,190,203,276,408,547,675,734],
  //         label: "Europe",
  //         borderColor: "#3cba9f",
  //         fill: false
  //       }, { 
  //         data: [40,20,10,16,24,38,74,167,508,784],
  //         label: "Latin America",
  //         borderColor: "#e8c3b9",
  //         fill: false
  //       }, { 
  //         data: [6,3,2,2,7,26,82,172,312,433],
  //         label: "North America",
  //         borderColor: "#c45850",
  //         fill: false
  //       }
  //     ]
  //   },
  //   options: {
  //     title: {
  //       display: true,
  //       text: 'PHASE 1: INSERSION'
  //     }
  //   }
  // });

  //For When you are doing this dynamicall

  //Get the number of goals (lets say for alacrity)

  //Then loop through and display a new one each time 

  //number = number of goals from database
  //for(var i = 0; i < number; i++)

  new Chart(document.getElementById("doughnut-chart"), {
      type: 'doughnut',
      data: {
        labels: [],
        datasets: [
          {
            label: "",
            backgroundColor: ["#EC008B", "#FFF"],
            borderWidth: ['0px', '0px'],
            data: [50,50]
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Predicted world population (millions) in 2050'
        },
        cutoutPercentage: 90
      }
  });

  new Chart(document.getElementById("doughnut-chart2"), {
      type: 'doughnut',
      data: {
        labels: [],
        datasets: [
          {
            label: "",
            backgroundColor: ["#EC008B", "#FFF"],
            borderWidth: ['0px', '0px'],
            data: [80,20]
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Predicted world population (millions) in 2050'
        },
        cutoutPercentage: 90
      }
  });

   new Chart(document.getElementById("doughnut-chart3"), {
      type: 'doughnut',
      data: {
        labels: [],
        datasets: [
          {
            label: "",
            backgroundColor: ["#EC008B", "#FFF"],
            borderWidth: ['0px', '0px'],
            data: [70,30]
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Predicted world population (millions) in 2050'
        },
        cutoutPercentage: 90
      }
  });

    new Chart(document.getElementById("doughnut-chart4"), {
      type: 'doughnut',
      data: {
        labels: [],
        datasets: [
          {
            label: "",
            backgroundColor: ["#EC008B", "#FFF"],
            borderWidth: ['0px', '0px'],
            data: [50,50]
          }
        ]
      },
      options: {
        title: {
          display: false,
          text: 'Predicted world population (millions) in 2050'
        },
        cutoutPercentage: 90
      }
  });
}

