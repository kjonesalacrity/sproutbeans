<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
    	DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Learn Classes and Ids',
            'progress' => 0,
        ]);

        DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Work on presentation skills',
            'progress' => 0,
        ]);
    }
}
