<?php

use Illuminate\Database\Seeder;

class PhasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
		DB::table('phases')->insert([
            'name' => 'Phase 1 - Tech Bootcamp',
            'start_date' => '2018-09-03',
            'end_date' => '2018-10-05',
        ]);

        DB::table('phases')->insert([
            'name' => 'Phase 2 - Business Bootcamp',
            'start_date' => '2018-10-07',
            'end_date' => '2018-10-31',
        ]);

        DB::table('phases')->insert([
            'name' => 'Phase 3 - Project Starting',
            'start_date' => '2018-11-03',
            'end_date' => '2018-12-31',
        ]);   	
    }
}
