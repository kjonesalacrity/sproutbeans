<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Alacrity Admin',
            'email' => 'Alacrity Admin',
            'password' => bcrypt('alacrity2018'),
            'jellyName' => '',
            'jellyColor' => '',
            'jellyType' => '',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Kelly Jones',
            'email' => 'kjones@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'JBean',
            'jellyColor' => 'pink',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);
    }
}
