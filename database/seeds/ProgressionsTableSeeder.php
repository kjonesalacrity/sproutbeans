<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgressionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

  
    public function run()
    {    
    	DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 50,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 72,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 84,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 80,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 100,
           'updated' => '2018-10-28',
        ]);

    }
}
